#!/usr/bin/env bash

set -ueo pipefail

echo "Setting AppRole Vars"
# Will be injected here during docker build
export ROLE_ID=a3ff8b9d-b448-d5d1-58db-93a4790a01e8
