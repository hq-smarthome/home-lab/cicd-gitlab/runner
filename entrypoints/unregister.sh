#!/usr/bin/env bash

set -ueo pipefail

/usr/local/bin/gitlab-runner unregister \
  --name "${RUNNER_NAME}" \
  --config "${CONFIG_FILE}" \
  --url "${CI_SERVER_URL}"
