#!/usr/bin/env bash

set -ueo pipefail

cat << EOS
{
  "driver": {
    "name": "Proxima - Nomad Executor",
    "version": "v0.1.0"
  },
  "builds_dir": "/workspace/build/${CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID}/${CUSTOM_ENV_CI_PROJECT_PATH_SLUG}",
  "cache_dir": "/workspace/cache/${CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID}/${CUSTOM_ENV_CI_PROJECT_PATH_SLUG}"
}
EOS
