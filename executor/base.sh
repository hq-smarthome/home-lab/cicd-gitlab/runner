#!/usr/bin/env bash

set -ueo pipefail

EXECUTOR_ID="runner-$CUSTOM_ENV_CI_RUNNER_ID-project-$CUSTOM_ENV_CI_PROJECT_ID-concurrent-$CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID-$CUSTOM_ENV_CI_JOB_ID"

EXECUTOR_ALLOC_FILE="/tmp/gitlab-executor-${EXECUTOR_ID}.alloc"
EXECUTOR_JOB_FILE="/tmp/gitlab-executor-${EXECUTOR_ID}.job"
