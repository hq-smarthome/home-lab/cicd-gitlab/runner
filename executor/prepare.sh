#!/usr/bin/env bash

set -ueo pipefail

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${currentDir}/base.sh # Get variables from base.

set -eo pipefail

# trap any error, and mark it as a system failure.
trap "exit $SYSTEM_FAILURE_EXIT_CODE" ERR

waitForRunningAllocation() {
  echo "Dispatch ID: $1"

  jobStatus=$(nomad job status -namespace cicd-gitlab "$dispatchId")
  if [ $? -ne 0 ]; then
    echo 'Failed to get job status. Exiting...'
    # Inform GitLab Runner that this is a system failure, so it
    # should be retried.
    exit "$SYSTEM_FAILURE_EXIT_CODE"
  fi

  echo "$jobStatus"
  echo "---"
  echo "Waiting for a running allocation..."
  for i in $(seq 1 60); do
    allocations=$(nomad alloc status -namespace cicd-gitlab -t "{{ range . }}{{ if (eq .JobID \"$dispatchId\") }}{{ printf \"%s %s\n\" .ID .ClientStatus }}{{ end }}{{ end }}")

    if [ "$(echo "$allocations" | grep 'running' | wc -l)" == "1" ]; then
      allocationId=$(echo "$allocations" | grep 'running' | cut -d ' ' -f1)

      echo "Attempt $i: Running allocation found."
      echo "Allocation ID: $allocationId"

      echo "$allocationId" > "$EXECUTOR_ALLOC_FILE"
      echo "$dispatchId" > "$EXECUTOR_JOB_FILE"

      break
    fi

    if [ "$(echo "$allocations" | grep 'failed' | wc -l)" == "1" ]; then
        echo "Attempt $i: Allocation failed to start."
        echo "---"
        echo 'Executor failed to start. exiting...'
        exit "$SYSTEM_FAILURE_EXIT_CODE"
    fi

    if [ "$i" == "60" ]; then
        echo "---"
        echo 'Waited for 300 seconds for allocation to be ready. exiting..'
        # Inform GitLab Runner that this is a system failure, so it
        # should be retried.
        exit "$SYSTEM_FAILURE_EXIT_CODE"
    fi

    echo "Attempt $i: No running allocations found. Waiting for 5 seconds..."

    sleep 5s
  done

  echo "---"
  echo "Executor ready"
}

dispatchJob() {
  dispatchOutput=$(nomad job dispatch -namespace cicd-gitlab -idempotency-token "$EXECUTOR_ID" -meta "executor_id=$EXECUTOR_ID" cicd-gitlab-linux-amd64-executor)
  if [ $? -ne 0 ]; then
    echo 'Failed to dispatch executor. Exiting...'
    # Inform GitLab Runner that this is a system failure, so it
    # should be retried.
    exit "$SYSTEM_FAILURE_EXIT_CODE"
  fi

  dispatchId=$(echo "$dispatchOutput" | grep 'Dispatched Job ID' | cut -d "=" -f2 | awk '{$1=$1};1')
  if [ "$dispatchId" == "" ]; then
    echo 'Failed to get dispatch ID. Exiting...'
    # Inform GitLab Runner that this is a system failure, so it
    # should be retried.
    exit "$SYSTEM_FAILURE_EXIT_CODE"
  fi

  waitForRunningAllocation "$dispatchId"
}

dispatchJob
