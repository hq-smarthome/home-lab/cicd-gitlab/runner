FROM registry.gitlab.com/hq-smarthome/home-lab/docker-base/proxima-alpine:latest
LABEL service=gitlab-runner

ARG BUILD_OS
ARG BUILD_ARCH
ARG NOMAD_VERSION=1.2.3
ARG VAULT_VERSION=1.9.1

RUN apk add bash --no-cache --no-progress

RUN addgroup -S gitlab-runner && adduser -S gitlab-runner -G gitlab-runner

# vault
RUN curl -L -o /tmp/vault.zip "https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_${BUILD_OS}_${BUILD_ARCH}.zip" && \
    unzip /tmp/vault.zip vault -jd /usr/local/bin && \
    chmod +x /usr/local/bin/vault && \
    rm /tmp/vault.zip

# nomad
RUN curl -L -o /tmp/nomad.zip "https://releases.hashicorp.com/nomad/${NOMAD_VERSION}/nomad_${NOMAD_VERSION}_${BUILD_OS}_${BUILD_ARCH}.zip" && \
    unzip /tmp/nomad.zip nomad -jd /usr/local/bin && \
    chmod +x /usr/local/bin/nomad && \
    rm /tmp/nomad.zip

RUN ls -la /usr/local/bin
RUN /usr/local/bin/vault -version
RUN /usr/local/bin/nomad -version

# # Install gitlab runner
# RUN curl -L -o /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-${BUILD_OS}-${BUILD_ARCH}" && \
#     chmod +x /usr/local/bin/gitlab-runner

# RUN mkdir /home/gitlab-runner/workspace && \
#     chown --recursive gitlab-runner:gitlab-runner /home/gitlab-runner/workspace
# COPY --chown=gitlab-runner:gitlab-runner ./executor /home/gitlab-runner/executor
# RUN chmod +x /home/gitlab-runner/executor/*.sh

# COPY ./entrypoints /entrypoints
# RUN chmod +x /entrypoints/*.sh

# STOPSIGNAL SIGTERM
