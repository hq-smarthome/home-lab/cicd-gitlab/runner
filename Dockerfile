FROM registry.gitlab.com/hq-smarthome/home-lab/docker-base/proxima-ubuntu-base:latest
LABEL service=gitlab-runner

ARG DEBIAN_FRONTEND=noninteractive
ARG ROLE_ID

# Add Hashicorp APT Repository and install dependencies
RUN curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add - && \
    apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main" && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
      nomad \
      vault \
      apt-transport-https \
      git \
      git-lfs \
      dumb-init && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    setcap cap_ipc_lock= /usr/bin/vault

RUN curl -L -o /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64" && \
    chmod +x /usr/local/bin/gitlab-runner

RUN useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

RUN mkdir -p /home/gitlab-runner/workspace && \
    chown gitlab-runner:gitlab-runner /home/gitlab-runner/workspace

COPY --chown=gitlab-runner:gitlab-runner ./executor /home/gitlab-runner/executor/
RUN chmod +x /home/gitlab-runner/executor/*.sh

COPY ./entrypoints /entrypoints/
RUN chmod +x /entrypoints/*.sh
# RUN echo "export ROLE_ID=${ROLE_ID}" >> /entrypoints/setEnvironment.sh

STOPSIGNAL SIGTERM

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
